<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostsController extends Controller
{
    public function postCreatePost(Request $request){

        $this->validate($request, [
            'text'=> 'required'
        ]);
        $post = new Post();
        $post->text= $request['text'];
        $request->user()->posts()->save($post);
        return redirect()->route('home')->with('success', 'Post Created');;
    }
    
    public function index(){
        
        $user=Auth::user();
        $posts=Post::all();
        return view('index', compact('user', 'posts'));
    }

    public function postEditPost($id){

        $user=Auth::user();

        $post = Post::find($id);

        if ($post->user == $user or $user->is_admin==1)
            return view('edit')->with('post', $post);
        else
            return redirect()->route('home')->with('error', 'You can edit only your own posts !!!');
        
    }

    public function postUpdatePost(Request $request){
        $id = $request->input('id');
        $post=Post::find($id);
        if (Auth::user() == $post->user or Auth::user()->is_admin==1){

       
            $this->validate($request, [
                'text'=> 'required'
            ]);


            $post->text= $request->input('text');
            $post->save();

            return redirect ('/posts')->with('success', 'Post updated');
        }else
            return redirect()->route('home')->with('error', 'You can edit only your own posts !!!');
    }

    public function postDeletePost(Request $request){

        $id = $request->input('id');
        $post=Post::find($id);

        if (Auth::user() == $post->user or Auth::user()->is_admin==1){
            $post->delete();

            return redirect ('/posts')->with('success', 'Post deleted');
        }else{
            return redirect()->route('home')->with('error', 'You can delete only your own posts !!!');
        }

    }
}
