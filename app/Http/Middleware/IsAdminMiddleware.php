<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        public function handle($request, Closure $next) { if(!Auth::check()){ return redirect()->route('login'); }

        if (Auth::user() &&  Auth::user()->is_admin == 1) {
            return $next($request);
       }

       return redirect('home')->with('error','You have not admin access');
        }

    }
}
