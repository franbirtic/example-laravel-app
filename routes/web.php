<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'myprofile'])->name('profile')->middleware('auth');

Route::post('/createpost', [App\Http\Controllers\PostsController::class, 'postCreatePost'])->name('post.create')->middleware('auth');
Route::get('post/{id}/editpost', [App\Http\Controllers\PostsController::class, 'postEditPost'])->name('post.edit')->middleware('auth');
Route::post('/update', [App\Http\Controllers\PostsController::class, 'postUpdatePost'])->name('post.update')->middleware('auth');
Route::post('/delete', [App\Http\Controllers\PostsController::class, 'postDeletePost'])->name('post.delete')->middleware('auth');
Route::get('/posts', [App\Http\Controllers\PostsController::class, 'index'])->name('index');
