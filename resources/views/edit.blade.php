@extends('layouts.app')

@section('content')

<div class="col-4">
    <h3>Edit post</h3>
    {{$post->text}}
    <form action="{{ route('post.update' , $post->id)}}" method="POST">
        
        <div class="form-group">
            <textarea name="text" id="text" cols="30" rows="10" placeholder ="Your input"></textarea>
        </div>
        <button type="submit" class="btn btn-primaty">Edit Post</button>
        <input type="hidden" value="{{$post->id}}" name="id">

        <input type="hidden" value="{{ Session::token() }}" name="_token">
    </form>
    
</div>

@endsection