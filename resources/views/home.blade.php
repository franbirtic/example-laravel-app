@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row">
        <div class="col-4">
            <img src="https://www.google.hr/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png" style="height:100px;">
        </div>
        <div class="col-4">
            
        </div>
        <div class="col-4">
            <img src="https://miro.medium.com/max/1200/0*UEtwA2ask7vQYW06.png" style="height:100px;">
        </div>
   </div>
   <div class="row pt-5">
        <div class="col-4">
            <h3>Type here</h3>
            <form action="{{ route('post.create')}}" method="POST">
                <div class="form-group">
                    <textarea name="text" id="text" cols="30" rows="10" placeholder ="Your input"></textarea>
                </div>
                <button type="submit" class="btn btn-primaty">Create Post</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
           
        </div>
        <div class="col-4">


            <a href="{{ url('/posts') }}"><button type="button" class="btn btn-primary">Show added content</button></a>
        </div>
        <div class="col-4">
        <a href="{{ url('profile/') }}"><button type="button" class="btn btn-primary">Show my profile</button></a>
        </div>
   </div>
</div>
@endsection
