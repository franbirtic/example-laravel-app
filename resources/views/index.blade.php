@extends('layouts.app')

@section('content')
<a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
@if ( Auth::user()->is_admin==1)  
    <h3>Pozdrav, Administratore/ice</h3>
@endif

            @if ($posts->count()>0)
                <h3>There are {{$posts->count()}} posts</h3> 
                @foreach ($posts as $post)
                    Author: {{$post->user->username}} <br>
                    Description: {{$post->text}} 
                    <br>

                    @if (($post->user == $user) or (Auth::user()->is_admin==1) )

                        <a href="post/{{$post->id}}/editpost"><button>Edit post</button></a>


                        <form action="{{ route('post.delete' , $post->id)}}" method="POST">
                            <input type="hidden" value="{{$post->id}}" name="id">
                            <input type="hidden" value="{{ Session::token() }}" name="_token">
                            <button type="submit">Delete post</button>
                        </form>
                    @endif
                    <hr>
                @endforeach
            @endif
@endsection
